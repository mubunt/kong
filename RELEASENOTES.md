# RELEASE NOTES: *kong*, A service console for server-type applications.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.6**:
  - Updated build system components.

- **Version 1.2.5**:
  - Updated build system.

- **Version 1.2.4**:
  - Removed unused files.

- **Version 1.2.3**:
  - Updated build system component(s)

- **Version 1.2.2**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.2.1**:
  - Some minor changes in .comment file(s).

- **Version 1.2.0**:
  - Completed completion with list of command candidates.
  - Added server status information at the beginning of the session.
  - Changed management of log viewer unicity.
  - Have slightly completed the README file

- **Version 1.1.0**:
  - Removed *mockup* and use of *kserver* as test vehicle.
  - Made some adjustments and fixes.

- **Version 1.0.0**:
  - First version.
