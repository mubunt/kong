 # *kong*, a service console for server-type applications.

The **kong** application allows you to manage a server type application in terms of starting, stopping, activating or stopping logs, status on its activity. **Kong** presents itself as a kind of shell with a few commands:

| Command | Description |
|---------|-------------|
| *exit*	| Terminate this shell process. Equivalent to 'quit'.
| *help* | List available commands.
| *history* | Print commands used in the session.
| *quit* | Terminate this shell process. Equivalent to 'exit'.
| *kill* | Stop non-responding server.
| *logon* | Start server log information.
| *logoff* | Stop server log information.
| *remote* | Status on server operation in a detached window.
| *showlog* | Show server log information
| *start* | Start server.
| *status* | Status on server operation.
| *stop* | Stop server.

The server that **kong** supports is instrumented so that the 2 applications can communicate. The [kserver](https://gitlab.com/mubunt/kserver) application used as a test vehicle provides an example of this instrumentation.

![Example](README_images/kong01.png  "Example")

The execution context of kong is defined in a configuration file to be specified during activation (argument --configuration / -c). Example:
``` text
; Configuration file example for kong.
; ===================================
[server]                                              ; SECTION server
name=KServer                                          ; Public name of server
executable=kserver                                    ; Executable name of the server (with path if needed)
options= --database=/home/michel/tmp/db --log=/tmp    ; Launching options of the server
[logs]                                                ; SECTION logs: how to viws server logs....
viewer=yaMore                                         ; utilituy to use to view logs
options=                                              ; Launching options of the viewer
lines=50                                              ; Geometry of the log viewer window
columns=80                                            ;
[geometry]                                            ; SECTION geometry of the initial kong window
lines=30                                              ;
columns=100                                           ;
```


## LICENSE
**kong** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ kong -h
kong - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
kong - Version 1.1.0

A service console for server-type applications

Usage: kong [OPTIONS]...

  -h, --help            Print help and exit
  -V, --version         Print version and exit
  -c, --configuration   Configuration file

$ kong -V
kong - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
kong - Version 1.1.0

$ kong -c configFile

```
## STRUCTURE OF THE APPLICATION
This section walks you through **kong**'s structure. Once you understand this structure, you will easily find your way around in **kong**'s code base.

``` bash
$ yaTree
./                              # Application level
├── README_images/              # Images for documentation
│   └── kong01.png              # 
├── src/                        # Source directory
│   ├── kong/                   # Top-level application
│   │   ├── Makefile            # Makefile
│   │   └── kong.c              # Main program source
│   ├── konginih/               # inih (INI Not Invented Here) file parser
│   │   ├── LICENSE.txt         # License text file
│   │   ├── Makefile            # Makefile
│   │   ├── README.md           # ReadMe Mark-Down file
│   │   ├── ini.c               # File parser source file
│   │   └── ini.h               # File parser header file
│   ├── kongnil/                # Remote status (do nothing, in fact it only provide a /dev/pts)
│   │   ├── Makefile            # Makefile
│   │   └── kongnil.c           # Main program source
│   ├── kongshell/              # Kong shell-like application
│   │   ├── Makefile            # Makefile
│   │   ├── kongcommand.c       # Command management, source file
│   │   ├── kongcommand.h       # Command management, header file
│   │   ├── kongconfiguration.c # Configuration management, source file
│   │   ├── kongconfiguration.h # Configuration management, header file
│   │   ├── kongextern.h        # External variable declarations
│   │   ├── konghistory.c       # History management, source file
│   │   ├── konghistory.h       # History management, header file
│   │   ├── konginput.c         # Input management, source file
│   │   ├── konginput.h         # Input management, header file
│   │   ├── kongscreen.c        # Screen management, source file
│   │   ├── kongscreen.h        # Screen management, header file
│   │   ├── kongshell.c         # Main program source
│   │   ├── kongshell.h         # Main header file
│   │   ├── kongterminal.c      # Terminal management, source file
│   │   └── kongterminal.h      # Terminal management, header file
│   ├── Makefile                # Makefile
│   └── kongdefs.h              # Overall application header file
├── COPYING.md                  # GNU General Public License markdown file
├── LICENSE.md                  # License markdown file
├── Makefile                    # Makefile
├── README.md                   # ReadMe markdown file
├── RELEASENOTES.md             # Release Notes markdown file
├── VERSION                     # Version identification text file
└── configFile                  # Example of configuration file

6 directories, 35 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd kong
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd kong
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage and development, nothing particular...
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***