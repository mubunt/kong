//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define CONSOLE 			KONGSHELL
#define SERVER 				configuration.execName

#define IPCKEY				1				// IPC key for the console
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_command {
	const char *name;
	bool (*exec)( void );
	const char *description;
};
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS HEADERS
//------------------------------------------------------------------------------
static bool commandQuit( void );
static bool commandHelp( void );
static bool commandHistory( void );
static bool commandKill( void );
static bool commandLogon( void );
static bool commandLogoff( void );
static bool commandStart( void );
static bool commandStatus( void );
static bool commandRemote( void );
static bool commandShowlog( void );
static bool commandStop( void );
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct s_command commands[] = {
	{ "exit", 		commandQuit,		"Terminate this shell process. Equivalent to 'quit'." },
	{ "help",		commandHelp,		"List available commands." },
	{ "history",	commandHistory,		"Print commands used in the session." },
	{ "quit",		commandQuit,		"Terminate this shell process. Equivalent to 'exit'." },
	{ "kill",		commandKill,		"Stop non-responding server." },
	{ "logon",		commandLogon,		"Start server log information." },
	{ "logoff",		commandLogoff,		"Stop server log information." },
	{ "remote",		commandRemote,		"Status on server operation in a detached window." },
	{ "showlog",	commandShowlog,		"Show server log information" },
	{ "start",		commandStart,		"Start server." },
	{ "status",		commandStatus,		"Status on server operation." },
	{ "stop",		commandStop,		"Stop server." }
};
static int			console_request;		// request identificator for console
static int			console_response;		// response identificator for server
static s_msg		conmsg;
static jmp_buf		jmpenv_buffer;
static int 			numberOfCommands;
static int 		*potentialities;
static const char 	*additional_help	= "Completion to helps users type their commands more quickly and easily, \n" \
        "is available. It does this by presenting possible options when users \n" \
        "press the Tab key while typing a command.";
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void getTimestamp( char *ts ) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ts, "%02d:%02d:%02d", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
}

static pid_t getPid( const char *process ) {
	char command[256];
	snprintf(command, sizeof(command), FORMAT_PIDOF, process);
	FILE *fdcmd = NULL;
	if (NULL == (fdcmd = popen(command, "r")))
		fatal_error("%s", "Cannot run 'pidof' command. Abort.");
	char spid[128];
	char *t = fgets(spid, sizeof(spid) - 1, fdcmd);
	pclose(fdcmd);
	if (t == NULL) return -1;
	return (pid_t) strtol(spid, (char **)NULL, 10);
}

static char *getUpTime( pid_t pid ) {
	char command[256];
	snprintf(command, sizeof(command), FORMAT_UPTIME, pid);
	FILE *fdcmd = NULL;
	if (NULL == (fdcmd = popen(command, "r")))
		fatal_error("%s", "Cannot run 'ps' command. Abort.");
	char *b = malloc(128);
	char *t = fgets(b, 127, fdcmd);
	t = fgets(b, 127, fdcmd);
	pclose(fdcmd);
	if (t == NULL) {
		free(b);
		return NULL;
	}
	if (b[strlen(b) - 1] == '\n') b[strlen(b) - 1] = '\0';
	return b;
}

static char *getContext( pid_t pid) {
	char *b = getUpTime(pid);
	if (b == NULL)
		return NULL;
	char *c = malloc(256);
	snprintf(c, 255, "Server %s is running since %s", configuration.name, b);
	free(b);
	return c;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static pid_t launch( const char *process, const char *options) {
	// Compute number of options
	char *copy = malloc(strlen(options) + 1);
	strcpy(copy, options);
	size_t number = 0;
	size_t maxlen = strlen(process);
	char *pt = strtok(copy, " ");
	while (pt != NULL) {
		maxlen = MAX(maxlen, strlen(pt));
		++number;
		pt = strtok(NULL, " ");
	}
	free(copy);
	number += 3; // +1 for "\0", +1 for exec name, + 1 for setsid
	// Allocation and initialization of argv
	char **argv = malloc(number * sizeof(char *));
	if (argv == NULL)
		fatal_error("%s", "Cannot allocate memory for server command. Abort.");
	argv[0] = (char *)"setsid";
	argv[1] = (char *)process;

	size_t j = 2;
	argv[j] = strtok((char *)options, " ");
	while (argv[j] != NULL) {
		++j;
		argv[j] = strtok(NULL, " ");
	}
	argv[number - 1] = NULL;
	// Launch
	pid_t pid;
	if ((pid = fork()) < 0) {			// fork a child process
		free(argv);
		ERROR("%s\n", "Cannot run server process...");
		return -1;
	}
	if (pid == 0) {						// for the child process
		close(1);						// close stdout => wriye to /dev/null
		close(2);						// close stderr => wriye to /dev/null
		execvp(*argv, argv);			// execute the command
		ERROR("Cannot run server process (errno='%s')...\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	free(argv);
	return pid;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void splitMessage(char *msg, char *key, int *steps) {
	char tmp[KEY_MAX_SIZE + MSG_MAX_SIZE + 1];
	char info[MSG_MAX_SIZE + 1];
	strncpy(tmp, msg, sizeof(tmp));
	(void) strtok(tmp, KEYSEP);
	if (msg[strlen(tmp)] == KEYSEP[0]) {
		strncpy(key, tmp, KEY_MAX_SIZE);
		strncpy(info, tmp + strlen(key) + 1, MSG_MAX_SIZE);
		*steps = (int) strtol(info, (char **)NULL, 10);
	} else {
		strncpy(key, tmp, KEY_MAX_SIZE);
		*steps = 0;
	}
}

static void splitMessageStr(char *msg, char *key, char *info) {
	char tmp[KEY_MAX_SIZE + MSG_MAX_SIZE + 1];
	strncpy(tmp, msg, sizeof(tmp));
	(void) strtok(tmp, KEYSEP);
	if (msg[strlen(tmp)] == KEYSEP[0]) {
		strncpy(key, tmp, KEY_MAX_SIZE);
		strncpy(info, tmp + strlen(key) + 1, MSG_MAX_SIZE);
	} else {
		strncpy(key, tmp, KEY_MAX_SIZE);
		*info = '\0';
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool kill_detached_status( void ) {
	bool b = false;
	if (fdevice != NULL) {
		fclose(fdevice);
		pid_t pid = getPid(KONGNIL);
		if (pid != -1) kill(pid, SIGTERM);
		b = true;
	}
	return b;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *acquisitionThread(void *arg  __attribute__((__unused__))) {
	char key[KEY_MAX_SIZE + 1];
	int additional_information;
	while (1) {
		SEND_COMMAND(console_request, CMD_ACQ, conmsg);
		READIPC(console_response, conmsg);
		splitMessage(conmsg.mtext, key, &additional_information);
		if (strcmp(key, CMD_ACQ) == 0) number_acquisition = (unsigned int)additional_information;
		usleep(POLLING_ACQUISITION);
	}
	// Never reached!
	pthread_exit(NULL);
}
static void *reportingThread(void *arg  __attribute__((__unused__))) {
	char key[KEY_MAX_SIZE + 1];
	int additional_information;
	while (1) {
		SEND_COMMAND(console_request, CMD_REP, conmsg);
		READIPC(console_response, conmsg);
		splitMessage(conmsg.mtext, key, &additional_information);
		if (strcmp(key, CMD_REP) == 0) number_report = (unsigned int)additional_information;
		usleep(POLLING_REPORTING);
	}
	// Never reached!
	pthread_exit(NULL);
}
static void *remotestatusThread(void *arg  __attribute__((__unused__))) {
	while (1) {
		screen_remote_indicators(2, 24);
		usleep(POLLING_REMOTESTATUS);
	}
	// Never reached!
	pthread_exit(NULL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *serverThread(void *arg  __attribute__((__unused__))) {
	char command[256];
	char timestamp[16];
	snprintf(command, sizeof(command), FORMAT_PIDOF " > /dev/null", configuration.execName);
	while (1) {
		bool initial_indicator_running = indicator_running;
		indicator_running = (0 == system(command));
		getTimestamp(timestamp);
		if (initial_indicator_running != indicator_running) {
			pthread_mutex_unlock(&screen_mutex);
			{
				(indicator_running)?
				screen_warning(POSITIVE, timestamp, "Server is running."):
				screen_warning(NEGATIVE, timestamp, "Server is stopped.");
				indicator_log_on = (indicator_running)?true:false;
			}
			pthread_mutex_unlock(&screen_mutex);
			if (indicator_running) {
				number_acquisition = number_report = 0;
				// Connection to server
				CREATEIPC(console_request, CONSOLE_REQUEST);
				CREATEIPC(console_response, CONSOLE_RESPONSE);
				conmsg.mtype = IPCKEY;
				// Is server acquiring data ?
				pthread_t acquiringongoing;
				if (pthread_create(&acquiringongoing, NULL, acquisitionThread, NULL) == -1)
					fatal_error("%s", "Cannot create acquisition activity thread. Abort.");
				// Is server reporting data ?
				pthread_t reportingongoing;
				if (pthread_create(&reportingongoing, NULL, reportingThread, NULL) == -1)
					fatal_error("%s", "Cannot create report activity thread. Abort.");
			} else {
				DELETEIPC(console_request);
				DELETEIPC(console_response);
				indicator_log_on = false;
			}
		}
		sleep(POLLING_SERVER_RUNNING);
	}
	// Never reached!
	pthread_exit(NULL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandQuit( void ) {
	bool b = kill_detached_status();
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandHelp( void ) {
	int numberOfCommands = (int)(sizeof(commands) / sizeof(commands[0]));
	for (int i = 0; i < numberOfCommands; i++)
		DISPLAY("\t%-10s %s\n", commands[i].name, commands[i].description);
	DISPLAY("\n%s\n\n", additional_help);
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandHistory( void ) {
	history_list();
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandKill( void ) {
	if (indicator_running) {
		pid_t pid = getPid(configuration.execName);
		if (pid != -1) {
			if (-1 == kill(pid, SIGTERM))
				ERROR("Cannot kill server process (%s)...\n", strerror(errno));
			else
				DISPLAY("Server (pid=%d) Killed.\n", pid);
		}
	} else
		ERROR("%s\n", "Server not running...");
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandLogon( void ) {
	if (indicator_running) {
		SEND_COMMAND2(console_request, CMD_LOGON, serverlogfile, conmsg);
		indicator_log_on = true;
		DISPLAY("Command LOG ON sent to server...\n");
	} else
		ERROR("%s\n", "Server not running...");
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandLogoff( void ) {
	if (indicator_running) {
		SEND_COMMAND(console_request, CMD_LOGOFF, conmsg);
		indicator_log_on = false;
		DISPLAY("Command LOG OFF sent to server...\n");
	} else
		ERROR("%s\n", "Server not running...");
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandShowlog( void ) {
	struct stat	locstat;			// stat variables for EXIST*
	struct stat	*ptlocstat = &locstat;
	if (indicator_running) {
		SEND_COMMAND(console_request, CMD_LOG, conmsg);
		READIPC(console_response, conmsg);
		char key[KEY_MAX_SIZE + 1];
		splitMessageStr(conmsg.mtext, key, serverlogfile);
	}
	if (strlen(serverlogfile) == 0)
		ERROR("%s\n", "Log file undefined...");
	else {
		if (EXISTFILE(serverlogfile)) {
			if (! terminal_viewerAlreadyRun(serverlogfile))
				terminal_viewlog(serverlogfile);
		} else
			ERROR("%s\n", "Non existing log file...");
	}
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandStart( void ) {
	if (indicator_running)
		ERROR("%s\n", "Server already running...");
	else {
		pid_t pid = launch(configuration.execName, configuration.execOption);
		if (pid > 0) {
			while (! indicator_running) sleep(WAIT_AFTER_START);
			SEND_COMMAND(console_request, CMD_LOG, conmsg);
			READIPC(console_response, conmsg);
			char key[KEY_MAX_SIZE + 1];
			splitMessageStr(conmsg.mtext, key, serverlogfile);
			DISPLAY("Server is now running (pid=%d). Log file is %s...\n", pid, serverlogfile);
		}
	}
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandStop( void ) {
	if (indicator_running) {
		SEND_COMMAND(console_request, CMD_STOP, conmsg);
		DISPLAY("Waiting for server shutdown.");
		pid_t pid;
		unsigned int k = 0;
		do {
			++k;
			DISPLAY(".");
			FLUSH();
			sleep(WAIT_AFTER_STOP);
			pid = getPid(configuration.execName);
		} while (pid != -1 && k < 5);
		if (pid == -1)
			DISPLAY("\nServer is now stopped...\n");
		else
			ERROR("\n%s\n", "Server is not responding. Cannot stop it...");
	} else
		ERROR("%s\n", "Server not running...");
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _sighandler(int sig) {
	signal(SIGINT, _sighandler);
	siglongjmp(jmpenv_buffer, -1);
}

static bool commandStatus( void ) {
	sighandler_t prevINT = signal(SIGINT, _sighandler);
	int row, col;
	HIDE_CURSOR();
	if ((0 == sigsetjmp(jmpenv_buffer, 1))) {
		pthread_mutex_lock(&screen_mutex);
		{
			DISPLAY(COLOR_HIT "Hit 'Ctrl C' to regain control" COLOR_STOP ENDOFLINE);
			DISPLAY_DASHLINE();
			DISPLAY(ENDOFLINE ENDOFLINE ENDOFLINE ENDOFLINE);
			DISPLAY_DASHLINE();
			FLUSH();
			SAVE_CURSOR();
			screen_getcursor(&row, &col);
			row -= 5;
			MOVE_CURSOR(row, 1);
			DISPLAY(COLOR_STATUS "%-20s" COLOR_STOP ENDOFLINE, "Server:");
			DISPLAY(COLOR_STATUS "%-20s" COLOR_STOP ENDOFLINE, "Data acquisition:");
			DISPLAY(COLOR_STATUS "%-20s" COLOR_STOP ENDOFLINE, "Data reporting:");
			DISPLAY(COLOR_STATUS "%-20s" COLOR_STOP ENDOFLINE, "Log:");
			RESTORE_CURSOR();
		}
		pthread_mutex_unlock(&screen_mutex);
		while (1) {
			pthread_mutex_lock(&screen_mutex);
			{
				screen_indicators(row, 21);
			}
			pthread_mutex_unlock(&screen_mutex);
			sleep(POLLING_STATUS);
		}
	}
	SHOW_CURSOR();
	MOVE_CURSOR(row + 4, 1);
	DISPLAY(ENDOFLINE);
	signal(SIGINT, prevINT);
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool commandRemote( void ) {
	bool threadExisting = kill_detached_status();
	terminal_viewstatus();
	pid_t pid = getPid(KONGNIL);
	if (pid == -1) {
		ERROR("Cannot get detached window process pid (%s)...\n", strerror(errno));
		return false;
	}
	char procPath[64];
	char device[128];
	sprintf(procPath, "/proc/%d/fd/0", pid);
	long int ret = readlink(procPath, device, 128);
	device[ret] = '\0';

	fdevice = fopen(device, "w");
	if (fdevice == NULL) {
		ERROR("Cannot open remote device (%s)...\n", strerror(errno));
	}
	if (! threadExisting) {
		pthread_t remotestatus;
		if (pthread_create(&remotestatus, NULL, remotestatusThread, NULL) == -1)
			fatal_error("%s", "Cannot create remote status thread. Abort.");
	}
	return false;
}
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
int command_analysis( const char *command) {
	int i = 0;
	int found = -1;
	int numberOfCommands = (int)(sizeof(commands) / sizeof(commands[0]));
	for (int i = 0; i < numberOfCommands; i++) {
		if (strcmp(commands[i].name, command) == 0) {
			found = i;
			break;
		}
	}
	return found;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool command_execute( int icommand) {
	bool b;
	pthread_mutex_unlock(&screen_mutex);
	{
		b = commands[icommand].exec();
	}
	pthread_mutex_unlock(&screen_mutex);
	return b;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const char *command_get( int icommand) {
	return commands[icommand].name;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void command_prepotential( void ) {
	numberOfCommands = (int)(sizeof(commands) / sizeof(commands[0]));
	potentialities = malloc((size_t)numberOfCommands * sizeof(int));
}

int command_potential( const char *command, int **candidate ) {
	memset(potentialities, 0, (size_t)numberOfCommands * sizeof(int));
	int numberOfPotentialities = 0;
	for (int i = 0; i < numberOfCommands; i++) {
		if (strncmp(commands[i].name, command, strlen(command)) == 0)
			potentialities[numberOfPotentialities++] = i;
	}
	*candidate = potentialities;
	return(numberOfPotentialities);
}

void command_postpotential( void ) {
	free(potentialities);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void command_server( void ) {
	pthread_t serverrunning;
	if (pthread_create(&serverrunning, NULL, serverThread, NULL) == -1)
		fatal_error("%s", "Cannot create server thread. Abort.");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void command_context( void ) {
	pid_t pid = getPid(configuration.execName);
	if (pid != -1) {
		char *b = getContext(pid);
		if (b == NULL)
			ERROR("Server %s is running but cannot get its uptime!!!", configuration.name);
		else {
			screen_frame(COLOR_HIT, "%s", b);
			free(b);
		}
	} else {
		screen_frame(COLOR_HIT, "Server %s is not running.", configuration.name);
	}
}
//------------------------------------------------------------------------------
