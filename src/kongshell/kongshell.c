//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define DISPLAY_BYE()	fprintf(stdout, "\r\nQuitting...\n")
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
size_t 					screen_nbLINES 			= 0;	// Number of lines of the terminal
size_t 					screen_nbCOLS 			= 0;	// Number of columns of the terminal
char 					*screen_screeninit;				// Startup terminal initialization
char 					*screen_screendeinit;			// Exit terminal de-initialization
char 					*screen_clear;					// Clear screen
char 					*screen_move;					// Cursor positioning
size_t					last_y_warning;
short unsigned int		given_lines				= 0;
short unsigned int		given_columns			= 0;
bool					indicator_running		= false;
bool					indicator_log_on		= false;
unsigned int			number_acquisition 		= 0;
unsigned int			number_report 			= 0;
s_config				configuration;
pthread_mutex_t 		screen_mutex			= PTHREAD_MUTEX_INITIALIZER;
char 					serverlogfile[PATH_MAX];
FILE 					*fdevice					= NULL;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void version(char *exec) {
	fprintf(stdout, "%s - %s\n%s - Version %s\n\n", KONGSHELL, COPYRIGHT, basename(exec), KONG_VERSION(VERSION));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void usage(char *exec) {
	version(exec);
	fprintf(stdout, "%s\n\nUsage: %s [OPTIONS]...\n\n", PURPOSE, basename(exec));
	fprintf(stdout, "  -h, --help            Print help and exit\n");
	fprintf(stdout, "  -V, --version         Print version and exit\n");
	fprintf(stdout, "  -c, --configuration   Configuration file\n");
	fprintf(stdout, "  -L, --lines           Numbers of lines to consider\n");
	fprintf(stdout, "  -C, --columns         Numbers of columns to consider\n");
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void cleanContext( int dummy __attribute__((__unused__)) ) {
	DISPLAY_BYE();
	sleep(WAIT_BEFORE_QUITTING);
	history_free();
	screen_resetContext();
	configuration_free();
	exit(EXIT_SUCCESS);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void getDate( char *ts ) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ts, "%02d-%02d-%02d", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat	locstat;				// stat variables for EXIST*
	struct stat	*ptlocstat=&locstat;	// stat variables for EXIST*
	//---- Option parsing ------------------------------------------------------
	struct option long_options[] = {
		{ "help",			no_argument,		0,	'h' },
		{ "version",		no_argument,		0,	'V' },
		{ "lines",			required_argument,	0,	'L' },
		{ "columns",		required_argument,	0,	'C' },
		{ "configuration",	required_argument,	0,	'c' },
		{ 0,				0,					0,	0 }
	};
	int option_index = 0;	// getopt_long stores the option index here.
	int opt;
	char *configfile = NULL;
	opterr = 0;
	while ((opt = getopt_long(argc, argv, "hVc:L:C:", long_options, &option_index)) != -1) {
		switch (opt) {
		case 0:	// If this option set a flag, do nothing else now.
			break;
		case 'h':	// Help
			usage(argv[0]);
			return EXIT_SUCCESS;
		case 'V':	// Version
			version(argv[0]);
			return EXIT_SUCCESS;;
		case 'L':
			given_lines = (short unsigned int) strtol(optarg, (char **)NULL, 10);
			if (given_lines <= 0) {
				fatal_error("%s", "Bad lines number. Abort.");
			}
			break;
		case 'C':
			given_columns = (short unsigned int) strtol(optarg, (char **)NULL, 10);
			if (given_columns <= 0) {
				fatal_error("%s", "Bad columns number. Abort.");
			}
			break;
		case 'c':
			configfile = optarg;
			break;
		case '?':	// getopt_long already printed an error message.
		default:
			return EXIT_FAILURE;
		}
	}
	if (configfile == NULL)
		fatal_error("%s", "No configuration file.");
	if (! EXISTFILE(configfile))
		fatal_error("Non existing configuration file '%s'", configfile);
	//----  Go on --------------------------------------------------------------
	configuration_get(configfile);

	signal(SIGHUP, cleanContext);
	signal(SIGABRT, cleanContext);
	signal(SIGTERM, cleanContext);
	signal(SIGQUIT, cleanContext);
	signal(SIGINT, SIG_IGN);

	screen_getTerminalCapabilities();
	screen_setContext(KONG_VERSION(VERSION));
	screen_mode_getEchoMode();
	history_init();
	command_server();
	command_context();
	command_prepotential();

	for (;;) {
		char *newcommand = input_command();
		history_record(newcommand);
		int j = command_analysis(newcommand);
		bool b = false;
		if (j < 0) {
			error("Unknown command '%s'", newcommand);
		} else
			b = command_execute(j);
		free(newcommand);
		if (b) break;
	}
	command_postpotential();
	//---- Exit ----------------------------------------------------------------
	cleanContext(0);
}
//------------------------------------------------------------------------------
