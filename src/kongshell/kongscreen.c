//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define MAX_SAVE_TERMIOS		10

#define XCORNER_TL				"╭"
#define XCORNER_TR				"╮"
#define XCORNER_BL				"╰"
#define XCORNER_BR				"╯"
#define XHORIZON				"─"
#define XVERTICAL				"│"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
struct termios 	echomode;
struct termios	savemode[MAX_SAVE_TERMIOS];
int 			isavemode;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool screen_getTerminalSize( size_t *lines, size_t *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		w.ws_row = w.ws_col = 0;
	if (w.ws_row == 0) {
		const char *pt = getenv("LINES");
		if (pt != NULL) w.ws_row = (short unsigned int) strtol(pt, (char **)NULL, 10);
		if (w.ws_row == 0) w.ws_row = given_lines;
	}
	if (w.ws_col == 0) {
		const char *pt = getenv("COLUMNS");
		if (pt != NULL) w.ws_col = (short unsigned int) strtol(pt, (char **)NULL, 10);
		if (w.ws_col == 0) w.ws_col = given_columns;
	}
	if (w.ws_row == 0 || w.ws_col == 0) return false;
	*lines = (size_t) w.ws_row;
	*cols = (size_t) w.ws_col;
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void screen_winch( int dummy __attribute__((__unused__)) ) {
	signal(SIGWINCH, SIG_IGN);
	if (! screen_getTerminalSize(&screen_nbLINES, &screen_nbCOLS))
		fatal_error("%s", "Cannot fetch terminal size. Abort!");
	signal(SIGWINCH, screen_winch);
	return;
}
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void screen_setContext( const char *version ) {
	screen_winch(SIGWINCH);
	DISPLAY("\n" COLOR_HEADER "%s - Version %s at your service." COLOR_STOP "\n\n", KONG, version);
	last_y_warning = screen_nbCOLS;
	isavemode = -1;
	return;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_resetContext( void ) {
//	FLUSH();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		fatal_error("%s", "The terminfo database could not be found. Abort!");
		return;
	case 0:
		fatal_error("%s", "There is no entry for this terminal in the terminfo database. Abort!");
		return;
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (screen_screeninit = tgetstr("ti", &sp))) {
		fatal_error("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database. Abort!");
		return;
	}
	if (NULL == (screen_screendeinit = tgetstr("te", &sp))) {
		fatal_error("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database. Abort!");
		return;
	}
	if (NULL == (screen_clear = tgetstr("cl", &sp))) {
		fatal_error("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database. Abort!");
		return;
	}
	if (NULL == (screen_move = tgetstr("cm", &sp))) {
		fatal_error("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database. Abort!");
		return;
	}
	return;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_mode_getEchoMode( void ) {
	tcgetattr(STDIN_FILENO, &echomode);
}
void screen_mode_setEchoMode( void ) {
	tcsetattr(STDIN_FILENO, TCSANOW, &echomode);
}
void screen_mode_setRawMode( void ) {
	struct termios mode;
	tcgetattr(STDIN_FILENO, &mode);
	cfmakeraw(&mode);
	tcsetattr(STDIN_FILENO, TCSANOW, &mode);
}
void screen_mode_saveMode( void ) {
	++isavemode;
	if (isavemode == MAX_SAVE_TERMIOS)
		fatal_error("%s", "No more space for savind terminal mode structure. Abort.");
	tcgetattr(STDIN_FILENO, &savemode[isavemode]);
}
void screen_mode_restoreMode( void ) {
	tcsetattr(STDIN_FILENO, TCSANOW, &savemode[isavemode]);
	--isavemode;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_getcursor( int *line, int *col ) {
	char buf[16];
	screen_mode_saveMode();
	screen_mode_setRawMode();
	REQUEST_CURSOR_POSITION();
	ssize_t n = read(0, buf, sizeof(buf));
	sscanf(buf + 2, "%d;%dR", line, col);
	screen_mode_restoreMode();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_indicators( int row, int col ) {
	SAVE_CURSOR();
	MOVE_CURSOR(row, col);
	DISPLAY("%s%s%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, INDIC_PROCESS, COLOR_STOP);
	MOVE_CURSOR(row + 1, col);
	DISPLAY("%s%0*d%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, DIGITS_COMM, number_acquisition, COLOR_STOP);
	MOVE_CURSOR(row + 2, col);
	DISPLAY("%s%0*d%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, DIGITS_COMM, number_report, COLOR_STOP);
	MOVE_CURSOR(row + 3, col);
	DISPLAY("%s%s%s", (indicator_log_on)?COLOR_RUNNING:COLOR_STOPPED, (indicator_log_on)?"ON":"OFF", COLOR_STOP);
	RESTORE_CURSOR();
	FLUSH();
}

void screen_remote_indicators( int row, int col ) {
	REMOTE_MOVE_CURSOR(row, col);
	REMOTE_DISPLAY("%s%s%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, INDIC_PROCESS, COLOR_STOP);
	REMOTE_MOVE_CURSOR(row + 1, col);
	REMOTE_DISPLAY("%s%0*d%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, DIGITS_COMM, number_acquisition, COLOR_STOP);
	REMOTE_MOVE_CURSOR(row + 2, col);
	REMOTE_DISPLAY("%s%0*d%s", (indicator_running)?COLOR_RUNNING:COLOR_STOPPED, DIGITS_COMM, number_report, COLOR_STOP);
	REMOTE_MOVE_CURSOR(row + 3, col);
	REMOTE_DISPLAY("%s%s%s", (indicator_log_on)?COLOR_RUNNING:COLOR_STOPPED, (indicator_log_on)?"ON ":"OFF", COLOR_STOP);
	REMOTE_FLUSH();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_warning( warning w, const char *timestamp, const char *message ) {
#define WL 	(int)screen_nbLINES
	SAVE_CURSOR();
	MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(WL, (int)last_y_warning);
	last_y_warning = screen_nbCOLS - strlen(message) - strlen(timestamp);
	MOVE_CURSOR(WL, last_y_warning);
	(w == POSITIVE)?setColor(stdout, COLOR_WARNING_P):setColor(stdout, COLOR_WARNING_N);
	DISPLAY("%s %s", timestamp, message);
	resetColor(stdout);
	RESTORE_CURSOR();
	FLUSH();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void screen_frame( const char *color, const char *format, const char *msg ) {
	char b[1024];
	snprintf(b, sizeof(b) - 1, format, msg);
	DISPLAY("%s" XCORNER_TL, color);
	for (size_t i = 0; i < strlen(b) + 2; i++) DISPLAY(XHORIZON);
	DISPLAY(XCORNER_TR "\n");
	DISPLAY(XVERTICAL " " "%s" " " XVERTICAL "\n", b);
	DISPLAY(XCORNER_BL);
	for (size_t i = 0; i < strlen(b) + 2; i++) DISPLAY(XHORIZON);
	DISPLAY(XCORNER_BR COLOR_STOP "\n\n");
}
//------------------------------------------------------------------------------
