//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------
#ifndef KONGSHELL_H
#define KONGSHELL_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define WAIT_BEFORE_QUITTING				2
#define WAIT_BEFORE_QUITTING_ON_ERROR		5
#define POLLING_SERVER_RUNNING				2
#define POLLING_STATUS						1
#define WAIT_AFTER_STOP						2
#define WAIT_AFTER_START					1

#define POLLING_ACQUISITION					500000		// Micro-seconds -> 0.5 seconds
#define POLLING_REPORTING					1000000		// Micro-seconds -> 0.5 seconds
#define POLLING_REMOTESTATUS				500000

#define	DEFAULT_LOGLINES					20
#define DEFAULT_LOGCOLUMNS					80
#define DEFAULT_LOGVIEWER					"less"
#define DEFAULT_LOGVIEWEROPTIONS			""

#define error(_FMT, ...)					do { \
												setColor(stderr, COLOR_ERROR); \
												fprintf(stderr, "ERROR: " _FMT "\n", __VA_ARGS__); \
												resetColor(stderr); \
											} while (0)
#define fatal_error(_FMT, ...)				do { \
												setColor(stderr, COLOR_ERROR); \
												fprintf(stderr, "ERROR: " _FMT "\n\n", __VA_ARGS__); \
												resetColor(stderr); \
												sleep(WAIT_BEFORE_QUITTING_ON_ERROR); \
												exit(EXIT_FAILURE); \
											} while (0)
#define CREATEIPC(id, key) 					if ((id = msgget(key, S_IRWXU | IPC_CREAT)) == -1) \
												fatal_error("Cannot create new message queue (errno='%s')", strerror(errno));
#define DELETEIPC(id)						msgctl(id, IPC_RMID, IPC_PRIVATE);
#define	WRITEIPC(id, msg)					int _n = msgsnd(id, &msg, strlen(msg.mtext), IPC_PRIVATE)
#define READIPC(id, msg) 					do { \
												ssize_t _n; \
												while (((_n = msgrcv(id, &msg, MSG_MAX_SIZE, 0, IPC_PRIVATE)) == -1) && errno == 4); \
												msg.mtext[_n] = '\0'; \
											} while (0)
#define SEND_COMMAND(id, cmd, msg)			do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s", cmd); \
												WRITEIPC(id, msg); \
											} while (0)
#define SEND_COMMAND2(id, cmd, param, msg)	do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s:%s", cmd, param); \
												WRITEIPC(id, msg); \
											} while (0)
#define SEND_DATA(id, cmd, param, msg)		do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s:%d", cmd, param); \
												WRITEIPC(id, msg); \
											} while (0)
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct s_config {
	const char *name;
	const char *execName;
	const char *execOption;
	const char *viewer;
	const char *viewerOption;
	int 		loglines;
	int 		logcolumns;
} s_config;
//------------------------------------------------------------------------------
#endif	// KONGSHELL_H
