//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------
#ifndef KONGSCREEN_H
#define KONGSCREEN_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define MOVE_CURSOR(x, y)							tputs(tgoto(screen_move, (int)(y - 1), (int)(x - 1)), 1, putchar)
// To migrate to portable mechanism (terminfo, tputs, ...) later!!!
#define HIDE_CURSOR()								DISPLAY("\033[?25l")
#define SHOW_CURSOR()								DISPLAY("\033[?25h")
#define MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(x, y)		DISPLAY("\033[%d;%dH\033[K", x, y)
#define CLEAR_CURRENT_LINE()						DISPLAY("\033[2K")
#define REQUEST_CURSOR_POSITION()					DISPLAY("\033[6n"); FLUSH()
#define SAVE_CURSOR()								DISPLAY("\0337")
#define RESTORE_CURSOR()							DISPLAY("\0338")
#define DISPLAY_BELL()								do { \
														DISPLAY("\033[?5h"); FLUSH(); \
														usleep(50000); \
														DISPLAY("\033[?5l"); FLUSH(); \
													} while (0)
#define MOVE_CURSOR_BACK(n)							for (size_t __i = 0; __i < n; __i++) { \
														DISPLAY("\b"); \
														CLEAR_CURRENT_LINE(); \
													}
#define DISPLAY_DASHLINE()							do { \
														for (size_t __i = 0; __i < screen_nbCOLS - 1; __i++) { \
															DISPLAY("─"); \
														} \
														DISPLAY(ENDOFLINE); \
													} while (0)
#define DISPLAY_PROMPT()							do { \
														time_t __rawtime; \
														struct tm * __timeinfo; \
														time(&__rawtime); \
														__timeinfo = localtime(&__rawtime); \
														DISPLAY("%02d:%02d:%02d$ ", __timeinfo->tm_hour, __timeinfo->tm_min, __timeinfo->tm_sec); \
													} while (0)

#define REMOTE_DISPLAY(...)							fprintf(fdevice, __VA_ARGS__)
#define REMOTE_FLUSH()								fflush(fdevice)
#define REMOTE_HIDE_CURSOR()						REMOTE_DISPLAY("\033[?25l")
#define REMOTE_MOVE_CURSOR(x, y)					REMOTE_DISPLAY("\033[%d;%df", x, y)
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS DEFINITIONS
//------------------------------------------------------------------------------
typedef enum { STOPPED, RUNNING } process;
typedef enum { POSITIVE, NEGATIVE } warning;
//------------------------------------------------------------------------------
// ROUTINES
//------------------------------------------------------------------------------
extern void		screen_setContext( const char * );
extern void		screen_resetContext( void );
extern void 	screen_getTerminalCapabilities( void );
extern void		screen_getcursor( int *, int * );
extern void		screen_indicators( int, int );
extern void		screen_remote_indicators( int, int );
extern void		screen_warning( warning, const char *, const char * );
extern void 	screen_mode_getEchoMode( void );
extern void 	screen_mode_setEchoMode( void );
extern void		screen_mode_setRawMode( void );
extern void 	screen_mode_returnMode( void );
extern void 	screen_mode_saveMode( void );
extern void		screen_mode_restoreMode( void );
extern void		screen_frame( const char *, const char *, const char * );
//------------------------------------------------------------------------------
#endif	// KONGSCREEN_H
