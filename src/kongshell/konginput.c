//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define TAB 				0x09
#define CR 					0x0d
#define ESCAPE				0x1b
#define ARROW_UP			0x41
#define ARROW_DOWN			0x42
#define SQUARE				0x5b
#define BACKSPACE 			0x7f
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
char *newcmd = NULL;
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void input_refresh( void ) {
	DISPLAY_PROMPT();
	if (newcmd != NULL && *newcmd != '\0') {
		setColor(stdout, COLOR_COMMAND);
		DISPLAY("%s", newcmd);
		resetColor(stdout);
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char *input_command( void ) {
	newcmd = malloc(MAXCMDLEN);

	while (1) {
		int row, col;
		char *pth;
		char *pt = newcmd;
		int *candidate = NULL;
		*pt = '\0';
		int ipotential = 0;
		screen_mode_setRawMode();
		while (1) {
			int c;
			pthread_mutex_lock(&screen_mutex);
			{
				screen_getcursor(&row, &col);
				MOVE_CURSOR(row, 1);
				CLEAR_CURRENT_LINE();
				DISPLAY_PROMPT();
				setColor(stdout, COLOR_COMMAND);
				DISPLAY("%s", newcmd);
			}
			pthread_mutex_unlock(&screen_mutex);
			c = getchar();
			pthread_mutex_lock(&screen_mutex);
			{
				resetColor(stdout);
				FLUSH();
			}
			pthread_mutex_unlock(&screen_mutex);
			switch (c) {
			case CR:
				*pt = '\0';
				break;
			case TAB:
				switch (ipotential) {
				case 0:
					DISPLAY_BELL();
					break;
				case 1:
					strcpy(newcmd, command_get(candidate[0]));
					pt = newcmd + strlen(newcmd);
					break;
				default:
					DISPLAY(ENDOFLINE);
					for (int k = 0; k < ipotential; k++)
						DISPLAY(COLOR_HIT "%-10s" COLOR_STOP, command_get(candidate[k]));
					DISPLAY(ENDOFLINE);
					break;
				}
				break;
			case BACKSPACE:
				if (pt == newcmd) {
					DISPLAY_BELL();
				} else {
					MOVE_CURSOR_BACK(1);
					--pt;
					*pt = '\0';
				}
				break;
			case ESCAPE:
				switch (getchar()) {
				case SQUARE:
					c = getchar();
					switch (c) {
					case ARROW_UP:
						pth = history_up();
						if (pth == NULL)
							DISPLAY_BELL();
						else {
							strcpy(newcmd, pth);
							pt = newcmd + strlen(newcmd);
						}
						break;
					case ARROW_DOWN:
						pth = history_down();
						if (pth == NULL)
							DISPLAY_BELL();
						else {
							strcpy(newcmd, pth);
							pt = newcmd + strlen(newcmd);
						}
						break;
					default:
						DISPLAY_BELL();
						break;
					}
					break;
				default:
					DISPLAY_BELL();
					break;
				}
				break;
			default:
				*pt = (char)c;
				++pt;
				*pt = '\0';
				break;
			}
			if (c == CR) break;
			ipotential = command_potential(newcmd, &candidate);
		}
		screen_mode_setEchoMode();
		if (strlen(newcmd) == 0) {
			DISPLAY("\n");
			continue;
		}
		history_reset();
		pthread_mutex_lock(&screen_mutex);
		{
			screen_getcursor(&row, &col);
			MOVE_CURSOR(row, 1);
			CLEAR_CURRENT_LINE();
			input_refresh();
			DISPLAY(ENDOFLINE);
			FLUSH();
		}
		pthread_mutex_unlock(&screen_mutex);
		break;
	}
	return newcmd;
}
//------------------------------------------------------------------------------
