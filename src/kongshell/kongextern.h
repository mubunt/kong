//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------
#ifndef KONGSEXTERN_H
#define KONGSEXTERN_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <getopt.h>
#include <signal.h>
#include <termios.h>
#include <termcap.h>
#include <pthread.h>
#include <errno.h>
#include <setjmp.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kserver.h"

#include "kongdefs.h"
#include "kongshell.h"
#include "kongscreen.h"
#include "konginput.h"
#include "konghistory.h"
#include "kongconfiguration.h"
#include "kongterminal.h"
#include "kongcommand.h"
#include "ini.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES DEFINED IN kongshell.c
//------------------------------------------------------------------------------
extern size_t 				screen_nbLINES;				// Number of lines of the terminal
extern size_t 				screen_nbCOLS;				// Number of columns of the terminal
extern char 				*screen_screeninit;			// Startup terminal initialization
extern char 				*screen_screendeinit;		// Exit terminal de-initialization
extern char 				*screen_clear;				// Clear screen
extern char 				*screen_move;				// Cursor positioning
extern size_t				last_y_warning;
extern short unsigned int	given_lines;
extern short unsigned int	given_columns;
extern bool					indicator_running;
extern bool					indicator_log_on;
extern unsigned int			number_acquisition;
extern unsigned int			number_report;
extern s_config				configuration;
extern pthread_mutex_t 		screen_mutex;
extern char 				serverlogfile[];
extern FILE 				*fdevice;
//------------------------------------------------------------------------------
#endif	// KONGSEXTERN_H
