//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define COMMAND_LOG 		"xfce4-terminal" \
								" --geometry=%dx%d" \
								" --color-bg=" BACKCOLOR \
								" --color-text=" FORECOLOR \
								" --show-scrollbar" \
								" --title=\"%s\"" \
								" --command=\"bash -c \\\"export PATH=%s; export LINES=%d; export COLUMNS=%d; %s %s %s\\\"\""
#define LINES_STATUS		"6"
#define COLUMNS_STATUS		"35"
#define COMMAND_STATUS		"xfce4-terminal" \
								" --geometry=" COLUMNS_STATUS "x" LINES_STATUS "+0+0" \
								" --color-bg=" BACKCOLOR \
								" --color-text=" FORECOLOR \
								" --title=\"%s\"" \
								" --hide-menubar" \
								" --hide-toolbar" \
								" --hide-scrollbar" \
								" --command=\"bash -c \\\"export PATH=%s; export LINES=" LINES_STATUS "; export COLUMNS=" COLUMNS_STATUS "; kongnil\\\"\""
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static char *can_run_command( const char *cmd ) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		if (access(cmd, X_OK) != 0) return NULL;
		char *buf = malloc(strlen(cmd) + 1);
		strcpy(buf, cmd);
		return buf;
	}
	const char *path = getenv("PATH");
	if (!path) return NULL; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path) + strlen(cmd) + 3);
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p == buf) *p++ = '.';
		// slash and command name
		if (p[-1] != '/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK) == 0) return buf;
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return NULL;
}
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void terminal_viewlog( const char *filelog ) {
	char *viewer = can_run_command(configuration.viewer);
	if (NULL == viewer)
		fatal_error("Wrong configuration. '%s' not found in $PATH.", configuration.viewer);
	const char *path = getenv("PATH");
	char *ptcommand = malloc(strlen(COMMAND_LOG) + strlen(KONGLOG)
	                         + strlen(viewer) + strlen(configuration.viewerOption) + strlen(filelog) + strlen(path)
	                         + 128);
	if (ptcommand == NULL)
		fatal_error("%s", "Cannot allocate memory for log viewer command.");
	// WARNING: With xfce4-terminal, I do not find a way to catch the size of the
	// distant window (see 'screen_getTerminalSize' routine in Kongshell that runs
	// very well with xterm). Oneway I find is to declare environment variables LINES
	// and COLUMNS using 'bash -c' colland. Another way is to pass the size as explicit
	// parameter.
	sprintf(ptcommand, COMMAND_LOG, configuration.logcolumns, configuration.loglines, \
	        KONGLOG, path, configuration.loglines, configuration.logcolumns, viewer, configuration.viewerOption, filelog);
	free(viewer);
	if (system(ptcommand) != 0)
		ERROR("Error during execution of '%s'.", ptcommand);
	free(ptcommand);
}

bool terminal_viewerAlreadyRun( const char *filelog ) {
	struct stat	locstat;			// stat variables for EXIST*
	struct stat	*ptlocstat = &locstat;
	char buffer[256];
	char *viewer = can_run_command(configuration.viewer);
	snprintf(buffer, sizeof(buffer) - 1, FORMAT_PIDOF, viewer);
	FILE *fdcmd = NULL;
	if (NULL == (fdcmd = popen(buffer, "r"))) {
		ERROR("%s", "Cannot run 'pidof' command.");
		return false;
	}
	char spid[128];
	char *t = fgets(spid, sizeof(spid) - 1, fdcmd);
	pclose(fdcmd);
	if (t == NULL) {
		ERROR("%s", "No result for 'pidof' buffer.");
		return false;
	}
	if (spid[strlen(spid) - 1] == '\n') spid[strlen(spid) - 1] = ' ';
	char *ptspid = spid;
	char *ptr;
	bool found = false;
	do {
		ptr = strchr(ptspid, ' ');
		if (ptr) {
			*ptr = '\0';
			pid_t pid = (pid_t) strtol(ptspid, (char **)NULL, 10);
			ptspid = ptr + 1;
			snprintf(buffer, sizeof(buffer) - 1, "/proc/%d/cmdline", pid);
			if (EXISTFILE(buffer)) {
				if (NULL == (fdcmd = fopen(buffer, "r"))) {
					ERROR("Cannot open file %s.", buffer);
					return false;
				}
				if (fread(buffer, 1, sizeof(buffer), fdcmd) > 0) {
					if (strcmp(buffer, viewer) == 0) {
						char *pt = buffer;
						while (*pt != '\0') ++pt;
						++pt;
						if (strlen(configuration.viewerOption) != 0) {
							if (strcmp(pt, configuration.viewerOption) == 0) {
								while (*pt != '\0') ++pt;
								++pt;
								if (strcmp(pt, filelog) == 0) {
									found = 1;
								}
							}
						} else {
							if (strcmp(pt, filelog) == 0) {
								found = 1;
							}
						}
					} else {
						ERROR("%s", "Not the right process.... Weird !!!");
					}
				}
				fclose(fdcmd);
				if (found) break;
			}
		}
	} while (ptr);
	return found;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void terminal_viewstatus( void ) {
	const char *path = getenv("PATH");
	char *ptcommand = malloc(strlen(COMMAND_STATUS) + strlen(KONGSTATUS) + strlen(path) + 64);
	if (ptcommand == NULL)
		fatal_error("%s", "Cannot allocate memory for status command.");
	sprintf(ptcommand, COMMAND_STATUS, KONGSTATUS, path);
	if (system(ptcommand) != 0)
		ERROR("Error during execution of '%s'.", ptcommand);
	free(ptcommand);
}
//------------------------------------------------------------------------------
