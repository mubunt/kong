//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define MATCH(s, n)		strcmp(section, s) == 0 && strcmp(name, n) == 0
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static int handler(void *user, const char *section, const char *name, const char *value) {
	if (MATCH("server", "name")) {
		if (configuration.name != NULL) free((char *)configuration.name);
		configuration.name = strdup(value);
	} else if (MATCH("server", "executable")) {
		if (configuration.execName != NULL) free((char *)configuration.execName);
		configuration.execName = strdup(value);
	} else if (MATCH("server", "options")) {
		if (configuration.execOption != NULL) free((char *)configuration.execOption);
		configuration.execOption = strdup(value);
	} else if (MATCH("logs", "viewer")) {
		if (configuration.viewer != NULL) free((char *)configuration.viewer);
		configuration.viewer = strdup(value);
	} else if (MATCH("logs", "options")) {
		if (configuration.viewerOption != NULL) free((char *)configuration.viewerOption);
		configuration.viewerOption = strdup(value);
	} else if (MATCH("logs", "lines")) {
		configuration.loglines = (int) strtol(value, (char **)NULL, 10);
	} else if (MATCH("logs", "columns")) {
		configuration.logcolumns = (int) strtol(value, (char **)NULL, 10);
	} else {
		return 0;  /* unknown section/name, error */
	}
	return 1;
}
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void configuration_get( const char *configFile ) {
	configuration.name = NULL;
	configuration.execName = NULL;
	configuration.execOption = NULL;
	configuration.viewer = strdup(DEFAULT_LOGVIEWER);
	configuration.viewerOption = strdup(DEFAULT_LOGVIEWEROPTIONS);
	configuration.loglines = DEFAULT_LOGLINES;
	configuration.logcolumns = DEFAULT_LOGCOLUMNS;
	if (ini_parse(configFile, handler, &configuration) < 0)
		fatal_error("Error when processing configuration file '%s'. Abort.", configFile);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void configuration_free( void ) {
	if (configuration.name != NULL) free((char *)configuration.name);
	if (configuration.execName != NULL) free((char *)configuration.execName);
	if (configuration.execOption != NULL) free((char *)configuration.execOption);
	if (configuration.viewer != NULL) free((char *)configuration.viewer);
	if (configuration.viewerOption != NULL) free((char *)configuration.viewerOption);
	configuration.name = NULL;
	configuration.execName = NULL;
	configuration.execOption = NULL;
	configuration.viewer = NULL;
	configuration.viewerOption = NULL;
}
//------------------------------------------------------------------------------
