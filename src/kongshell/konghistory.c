//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongextern.h"
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
struct s_history {
	char command[MAXCMDLEN];
	struct s_history *previous;
	struct s_history *next;
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
struct s_history	*history_top;
struct s_history	*history_current;
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void history_init( void ) {
	history_top = history_current = NULL;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char *history_up( void ) {
	if (history_current == NULL) return NULL;
	if (history_current->previous == NULL)
		return history_current->command;
	else {
		history_current = history_current->previous;
		return history_current->next->command;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char *history_down( void ) {
	if (history_current == NULL) return NULL;
	if (history_current->next == NULL) return NULL;
	history_current = history_current->next;
	return history_current->previous->command;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void history_record( char *command ) {
	if (history_top != NULL && strcmp(command, history_top->command) == 0) return;
	struct s_history *ptmp = malloc(sizeof(struct s_history) * sizeof(char));
	if (ptmp == NULL)
		fatal_error("%s", "ERROR: Cannot allocate memory for history...");
	strcpy(ptmp->command, command);
	ptmp->previous = history_top;
	ptmp->next = NULL;
	if (history_top != NULL) history_top->next = ptmp;
	history_top = history_current = ptmp;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void history_reset( void ) {
	history_current = history_top;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void history_list( void ) {
	struct s_history *ptmp = history_top;
	while (ptmp != NULL) {
		DISPLAY("\t%s\n", ptmp->command);
		ptmp = ptmp->previous;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void history_free( void ) {
	while (history_top != NULL) {
		struct s_history *ptmp = history_top;
		history_top = ptmp->previous;
		free(ptmp);
	}
}
//------------------------------------------------------------------------------
