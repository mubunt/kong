//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------
#ifndef KONGCOMMAND_H
#define KONGCOMMAND_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define FORMAT_PIDOF		"pidof -x %s"
#define FORMAT_UPTIME		"ps -p %d -o lstart"
//------------------------------------------------------------------------------
// ROUTINES
//------------------------------------------------------------------------------
extern int			command_analysis( const char * );
extern bool			command_execute( int );
extern const char 	*command_get( int );
extern void 		command_prepotential( void );
extern int			command_potential( const char *, int ** );
extern void 		command_postpotential( void );
extern void			command_server( void );
extern void			command_context( void );
//------------------------------------------------------------------------------
#endif	// KONGCOMMAND_H
