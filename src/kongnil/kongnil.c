//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongdefs.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define HIDE_CURSOR()								DISPLAY("\033[?25l")
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void screen_mode_setRawMode( void ) {
	struct termios mode;
	tcgetattr(STDIN_FILENO, &mode);
	cfmakeraw(&mode);
	tcsetattr(STDIN_FILENO, TCSANOW, &mode);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main() {
	screen_mode_setRawMode();
	HIDE_CURSOR();

	DISPLAY(ENDOFLINE);
	DISPLAY(COLOR_STATUS " %-22s" COLOR_STOP, "Server:");
	DISPLAY(COLOR_STOPPED "%s" COLOR_STOP ENDOFLINE, INDIC_PROCESS);
	DISPLAY(COLOR_STATUS " %-22s" COLOR_STOP, "Data acquisition:");
	DISPLAY(COLOR_STOPPED "%0*d" COLOR_STOP ENDOFLINE, DIGITS_COMM, 0);
	DISPLAY(COLOR_STATUS " %-22s" COLOR_STOP, "Data reporting:");
	DISPLAY(COLOR_STOPPED "%0*d" COLOR_STOP ENDOFLINE, DIGITS_COMM, 0);
	DISPLAY(COLOR_STATUS " %-22s" COLOR_STOP, "Log:");
	DISPLAY(COLOR_STOPPED "%s" COLOR_STOP ENDOFLINE, "OFF");
	FLUSH();
	//---- Infinite loop -------------------------------------------------------
	while (1)
		sleep(3600);
	//---- Exit ----------------------------------------------------------------
	// Never reached !
	return(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
