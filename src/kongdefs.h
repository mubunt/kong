//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------
#ifndef DEFSKONG_H
#define DEFSKONG_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define KONG				"kong"
#define KONGSHELL			"kongshell"
#define KONGNIL				"kongnil"

#define KONGLOG				"kong - Server Logs"
#define KONGSTATUS			"kong - Status"
#define PURPOSE				"A service console for server-type applications"
#define COPYRIGHT			"Copyright (c) 2020, Michel RIZZO. All Rights Reserved."

#ifdef VERSION
#define KONG_VERSION(x) 	str(x)
#define str(x)				#x
#else
#define KONG_VERSION 		"Unknown"
#endif

//#define BACKCOLOR			"#2F4F4F"	// DarkSlateGrey
//#define BACKCOLOR			"#000000"	// Black
//#define BACKCOLOR			"#696969"	// DimGrey
//#define BACKCOLOR			"#191970"	// MidnightBlue
#define BACKCOLOR			"#003333"	// DarkerTeal
#define FORECOLOR 			"#DCDCDC"	// Gainsboro

#define COLOR_HEADER		"\033[1;37m"	// Foreground: Bold White
#define COLOR_ERROR			"\033[1;31m"	// Foreground: Bold Red
#define COLOR_COMMAND		"\033[1;33m"	// User command: Bold Yellow
#define COLOR_STOPPED		"\033[31m"		// Process stopped: Red
#define COLOR_RUNNING		"\033[92m"		// Process on going: Dark Green
#define COLOR_WARNING_P		"\033[1;30;47m"	// Foreround: Bold Black. Background: White
#define COLOR_WARNING_N		"\033[1;37;41m"	// Foreround: Bold White. Background: Red
#define COLOR_HIT			"\033[3;37m"	// Foreground: Italic White
#define COLOR_STATUS		"\033[1;37m"	// Foreground: Bold White
#define COLOR_STOP			"\033[0m"		// No more escape code

#define EXISTFILE(x)		(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)

#define setColor(fd, color)	fprintf(fd, "%s", color)
#define resetColor(fd)		fprintf(fd, "%s", COLOR_STOP)

#define MAX(x, y)			(((x) > (y)) ? (x) : (y))
#define MIN(x, y)			(((x) < (y)) ? (x) : (y))

#define DISPLAY(...)		fprintf(stdout, __VA_ARGS__)
#define ERROR(fmt, ...)		fprintf(stdout, COLOR_ERROR fmt COLOR_STOP, __VA_ARGS__)
#define FLUSH()				fflush(stdout)
#define ENDOFLINE			"\r\n"
#define INDIC_PROCESS		"▁▂▃▄▅▆▇█"
#define DIGITS_COMM			6
//------------------------------------------------------------------------------
#endif	// DEFSKONG_H