//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kong
// A service console for server-type applications
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kongdefs.h"
#include "ini.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define error(_FMT, ...)	do { \
								setColor(stderr, COLOR_ERROR); \
								fprintf(stderr, "ERROR: " _FMT "\n\n", __VA_ARGS__); \
								resetColor(stderr); \
							} while (0)

#define NUMBERofLINES 		20
#define NUMBERofCOLUMNS 	100
#define COMMAND 			"xfce4-terminal --geometry=%dx%d" \
									" --color-bg=" BACKCOLOR " --color-text=" FORECOLOR \
									" --show-scrollbar --title=%s" \
									" --command=\"bash -c \\\"export PATH=%s; export LINES=%d; export COLUMNS=%d; %s --configuration=%s\\\"\""

#define MATCH(s, n)		strcmp(section, s) == 0 && strcmp(name, n) == 0
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct s_config {
	int lines;
	int columns;
} s_config;
//------------------------------------------------------------------------------
// GLOBALS VARIABLES
//------------------------------------------------------------------------------
static s_config configuration;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void version(char *exec) {
	fprintf(stdout, "%s - %s\n%s - Version %s\n\n", KONG, COPYRIGHT, basename(exec), KONG_VERSION(VERSION));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void usage(char *exec) {
	version(exec);
	fprintf(stdout, "%s\n\nUsage: %s [OPTIONS]...\n\n", PURPOSE, basename(exec));
	fprintf(stdout, "  -h, --help            Print help and exit\n");
	fprintf(stdout, "  -V, --version         Print version and exit\n");
	fprintf(stdout, "  -c, --configuration   Configuration file\n");
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char *can_run_command(const char *cmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		if (access(cmd, X_OK) != 0) return NULL;
		char *buf = malloc(strlen(cmd) + 1);
		strcpy(buf, cmd);
		return buf;
	}
	const char *path = getenv("PATH");
	if (!path) return NULL; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path) + strlen(cmd) + 3);
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p == buf) *p++ = '.';
		// slash and command name
		if (p[-1] != '/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK) == 0) return buf;
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return NULL;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int handler(void *user, const char *section, const char *name, const char *value) {
	if (MATCH("geometry", "lines")) {
		configuration.lines = (short unsigned int) strtol(value, (char **)NULL, 10);
	} else if (MATCH("geometry", "columns")) {
		configuration.columns = (short unsigned int) strtol(value, (char **)NULL, 10);
	} else {
		return 0;  /* unknown section/name, error */
	}
	return 1;
}

static bool configuration_get( const char *configFile ) {
	configuration.lines = NUMBERofLINES;
	configuration.columns = NUMBERofCOLUMNS;
	if (ini_parse(configFile, handler, &configuration) < 0) return false;
	return true;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat	locstat;				// stat variables for EXIST*
	struct stat	*ptlocstat=&locstat;	// stat variables for EXIST*
	//---- Option parsing ------------------------------------------------------
	struct option long_options[] = {
		{ "help",			no_argument,		0,	'h' },
		{ "version",		no_argument,		0,	'V' },
		{ "configuration",	required_argument,	0,	'c' },
		{ 0,				0,					0,	0 }
	};
	int option_index = 0;	// getopt_long stores the option index here.
	int opt;
	char *configfile = NULL;
	opterr = 0;
	while ((opt = getopt_long(argc, argv, "hVc:", long_options, &option_index)) != -1) {
		switch (opt) {
		case 0:	// If this option set a flag, do nothing else now.
			break;
		case 'h':	// Help
			usage(argv[0]);
			goto END;
		case 'V':	// Version
			version(argv[0]);
			goto END;
		case 'c':
			configfile = optarg;
			break;
		case '?':	// getopt_long already printed an error message.
		default:
			goto EXIT;
		}
	}
	if (configfile == NULL) {
		error("%s", "No configuration file.");
		usage(argv[0]);
		goto EXIT;
	}
	if (! EXISTFILE(configfile)) {
		error("Non existing configuration file '%s'", configfile);
		goto EXIT;
	}
	char *ptkongshell;
	if (NULL == (ptkongshell = can_run_command(KONGSHELL))) {
		error("Bad installation. '%s' not found in $PATH.", KONGSHELL);
		goto EXIT;
	}
	const char *path = getenv("PATH");
	char *ptcommand = (char *) malloc(strlen(COMMAND) + strlen(KONG) +
	                                  strlen(ptkongshell) + strlen(configfile) + strlen(path) + 64);
	if (ptcommand == NULL) {
		error("%s", "Cannot allocate memory for launching command.");
		free(ptkongshell);
		goto EXIT;
	}
	//---- Go on ----------------------------------------------------------------
	if (! configuration_get(configfile)) {
		error("Error when processing configuration file '%s'. Abort.", configfile);
		goto EXIT;
	}

	// WARNING: With xfce4-terminal, I do not find a way to catch the size of the
	// distant window (see 'screen_getTerminalSize' routine in Kongshell that runs
	// very well with xterm). Oneway I find is to declare environment variables LINES
	// and COLUMNS using 'bash -c' colland. Another way is to pass the size as explicit
	// parameter.
	sprintf(ptcommand, COMMAND, configuration.columns, configuration.lines, \
	        KONG, path, configuration.lines, configuration.columns, ptkongshell, configfile);
	free(ptkongshell);
	if (system(ptcommand) != 0) {
		error("Error during execution of '%s'.", ptcommand);
		free(ptcommand);
		goto EXIT;
	}
	free(ptcommand);
	//---- Exit ----------------------------------------------------------------
END:
	return EXIT_SUCCESS;
EXIT:
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
